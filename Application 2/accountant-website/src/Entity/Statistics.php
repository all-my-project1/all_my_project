<?php

namespace App\Entity;

use App\Repository\StatisticsRepository;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: StatisticsRepository::class)]
class Statistics
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $usersToday = 0;

    #[ORM\Column]
    private ?int $usersTotal = 0;

    #[ORM\Column]
    private ?int $clients = 0;

    #[ORM\Column]
    private ?int $clientsTotal = 0;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?DateTimeInterface $lastEdited;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsersToday(): ?int
    {
        return $this->usersToday;
    }

    public function setUsersToday(int $usersToday): self
    {
        $this->usersToday = $usersToday;

        return $this;
    }

    public function getUsersTotal(): ?int
    {
        return $this->usersTotal;
    }

    public function setUsersTotal(int $usersTotal): self
    {
        $this->usersTotal = $usersTotal;

        return $this;
    }

    public function getClients(): ?int
    {
        return $this->clients;
    }

    public function setClients(int $clients): self
    {
        $this->clients = $clients;

        return $this;
    }

    public function getClientsTotal(): ?int
    {
        return $this->clientsTotal;
    }

    public function setClientsTotal(int $clientsTotal): self
    {
        $this->clientsTotal = $clientsTotal;

        return $this;
    }

    public function getLastEdited(): ? DateTimeInterface
    {
        return $this->lastEdited;
    }
    public function setLastEdited(DateTimeInterface $lastEdited): self
    {
        $this->lastEdited = $lastEdited;
        return $this;
    }
}
