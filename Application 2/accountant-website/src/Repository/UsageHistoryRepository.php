<?php

namespace App\Repository;

use App\Entity\UsageHistory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<UsageHistory>
 *
 * @method UsageHistory|null find($id, $lockMode = null, $lockVersion = null)
 * @method UsageHistory|null findOneBy(array $criteria, array $orderBy = null)
 * @method UsageHistory[]    findAll()
 * @method UsageHistory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsageHistoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UsageHistory::class);
    }

    public function save(UsageHistory $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(UsageHistory $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }


    public function last7Days() : array
    {
        return $this->findBy([], ["id" => "ASC"], 7);
    }

    public function addData(int $views) : void
    {
        $u = new UsageHistory();
        $u->setViews($views);
        $u->setDate( new \DateTime(date('Y-m-d', strtotime("-1 day"))) );

        $this->save($u, true);
    }

//    /**
//     * @return UsageHistory[] Returns an array of UsageHistory objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('u')
//            ->andWhere('u.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('u.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?UsageHistory
//    {
//        return $this->createQueryBuilder('u')
//            ->andWhere('u.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
