<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Article>
 *
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    public function save(Article $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Article $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function search(string $query, int $pageSize, int $page): array
    {
        $query = strtolower($query);

        $qb = $this->createQueryBuilder('a');
        $qb->where($qb->expr()->like('a.title', ':q'))
            ->setParameter('q', '%' . $query . '%')
            ->orWhere($qb->expr()->like('a.content', ':c'))
            ->setParameter('c', '%' . $query . '%')
            ->setFirstResult($pageSize * ($page - 1)) // set the offset
            ->setMaxResults($pageSize);
        return $qb->getQuery()->getResult();
    }

    public function new_search($value, $status, $order, $limit, $offset, &$totalResults) : array
    {
        $value = strtolower($value);

        $qb = $this->createQueryBuilder('a');
        $qb->where($qb->expr()->orX(
            $qb->expr()->like('LOWER(a.title)',':value'),
            $qb->expr()->like('LOWER(a.content)',':value')
        ))
            ->setParameter('value', '%'.$value.'%')
            ->orderBy('a.date', $order)
            ->setFirstResult($offset)
            ->setMaxResults($limit);

        if ($status !== null) {
            $qb->andWhere($qb->expr()->eq('a.status', ':status'))
                ->setParameter('status', $status);
        }

        $result = $qb->getQuery()->getResult();

        $countQb = $this->createQueryBuilder('a');
        $countQb->select('COUNT(a.id)')
            ->where($countQb->expr()->orX(
                $countQb->expr()->like('LOWER(a.title)',':value'),
                $countQb->expr()->like('LOWER(a.content)',':value')
            ))
            ->setParameter('value', '%'.$value.'%');

        if ($status !== null) {
            $countQb->andWhere($countQb->expr()->eq('a.status', ':status'))
                ->setParameter('status', $status);
        }

        $totalResults = $countQb->getQuery()->getSingleScalarResult();

        return $result;
    }


//    /**
//     * @return Article[] Returns an array of Article objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('a.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Article
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
