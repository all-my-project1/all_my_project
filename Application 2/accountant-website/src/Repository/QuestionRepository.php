<?php

namespace App\Repository;

use App\Entity\Question;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Question>
 *
 * @method Question|null find($id, $lockMode = null, $lockVersion = null)
 * @method Question|null findOneBy(array $criteria, array $orderBy = null)
 * @method Question[]    findAll()
 * @method Question[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuestionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Question::class);
    }

    public function save(Question $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Question $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function search($value, $order, $limit, $offset, &$totalResults): array
    {
        $value = strtolower($value);

        $qb = $this->createQueryBuilder('q');
        $qb->where($qb->expr()->orX(
            $qb->expr()->like('LOWER(q.nickname)', ':value'),
            $qb->expr()->like('LOWER(q.email)', ':value'),
            $qb->expr()->like('LOWER(q.phone)', ':value'),
            $qb->expr()->like('LOWER(q.title)', ':value'),
            $qb->expr()->like('LOWER(q.content)', ':value')
        ))
            ->setParameter('value', '%' . $value . '%')
            ->orderBy('q.datetime', $order)
            ->setFirstResult($offset)
            ->setMaxResults($limit);

        $result = $qb->getQuery()->getResult();

        $countQb = $this->createQueryBuilder('q');
        $countQb->select('COUNT(q.id)')
            ->where($countQb->expr()->orX(
                $countQb->expr()->like('LOWER(q.nickname)', ':value'),
                $countQb->expr()->like('LOWER(q.email)', ':value'),
                $countQb->expr()->like('LOWER(q.phone)', ':value'),
                $countQb->expr()->like('LOWER(q.title)', ':value'),
                $countQb->expr()->like('LOWER(q.content)', ':value')
            ))
            ->setParameter('value', '%' . $value . '%');

        $totalResults = $countQb->getQuery()->getSingleScalarResult();

        return $result;
    }

//    /**
//     * @return Question[] Returns an array of Question objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('q')
//            ->andWhere('q.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('q.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Question
//    {
//        return $this->createQueryBuilder('q')
//            ->andWhere('q.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
