<?php

namespace App\Repository;

use App\Entity\Statistics;
use App\Entity\UsageHistory;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Statistics>
 *
 * @method Statistics|null find($id, $lockMode = null, $lockVersion = null)
 * @method Statistics|null findOneBy(array $criteria, array $orderBy = null)
 * @method Statistics[]    findAll()
 * @method Statistics[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatisticsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Statistics::class);
    }

    public function save(Statistics $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getInstance() : Statistics
    {
        $arr = $this->findAll();
        if(!empty($arr))
        {
            return $arr[0];
        }
        else
        {
            $s = new Statistics();
            $s->setLastEdited(new DateTime(date('Y-m-d')));
            $this->save($s, true);
            return $s;
        }

    }

    public function update(int $clients, int $clientsTotal) : void
    {
        $stats = $this->getInstance();

        $stats->setClients($clients);
        $stats->setClientsTotal($clientsTotal);

        $this->save($stats, true);
    }

    public function AddVisitor() : void
    {
        $stats = $this->getInstance();

        $now = new DateTime(date('Y-m-d'));
        $duration = ($stats->getLastEdited())->diff($now);
        dump($stats->getLastEdited());
        dump($duration);

        if($duration->d >= 1)
        {
            //set data to history
            $this->getEntityManager()->getRepository(UsageHistory::class)->addData($stats->getUsersToday());

            //erase
            $stats->setUsersToday(0);
        }

        //$diff = round($duration / (60 * 60 * 24));
        //heck if next day
        $stats->setUsersToday($stats->getUsersToday()+1);
        $stats->setUsersTotal($stats->getUsersTotal()+1);
        $stats->setLastEdited($now);

        $this->save($stats, true);

    }



//    /**
//     * @return Statistics[] Returns an array of Statistics objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Statistics
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
