<?php

namespace App\Repository;

use App\Entity\ContactUs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ContactUs>
 *
 * @method ContactUs|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContactUs|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContactUs[]    findAll()
 * @method ContactUs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactUsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ContactUs::class);
    }

    public function save(ContactUs $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ContactUs $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function search($value, $order, $limit, $offset, &$totalResults) : array
    {
        $value = strtolower($value);

        $qb = $this->createQueryBuilder('c');
        $qb->where($qb->expr()->orX(
            $qb->expr()->like('LOWER(c.name)',':value'),
            $qb->expr()->like('LOWER(c.email)',':value'),
            $qb->expr()->like('LOWER(c.phone)',':value')
        ))
            ->setParameter('value', '%'.$value.'%')
            ->orderBy('c.datetime', $order)
            ->setFirstResult($offset)
            ->setMaxResults($limit);

        $result = $qb->getQuery()->getResult();

        $countQb = $this->createQueryBuilder('c');
        $countQb->select('COUNT(c.id)')
            ->where($countQb->expr()->orX(
                $countQb->expr()->like('LOWER(c.name)',':value'),
                $countQb->expr()->like('LOWER(c.email)',':value'),
                $countQb->expr()->like('LOWER(c.phone)',':value')
            ))
            ->setParameter('value', '%'.$value.'%');

        $totalResults = $countQb->getQuery()->getSingleScalarResult();

        return $result;
    }


//    /**
//     * @return ContactUs[] Returns an array of ContactUs objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ContactUs
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
