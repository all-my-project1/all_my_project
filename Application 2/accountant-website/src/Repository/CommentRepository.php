<?php

namespace App\Repository;

use App\Entity\Comment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Comment>
 *
 * @method Comment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comment[]    findAll()
 * @method Comment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Comment::class);
    }

    public function save(Comment $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Comment $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function search($value, $order, $limit, $offset, &$totalResults) : array
    {
        $value = strtolower($value);

        $qb = $this->createQueryBuilder('c');

        $qb->join('c.user', 'u')
            ->where($qb->expr()->orX(
            $qb->expr()->like('LOWER(u.nickname)',':value'),
            $qb->expr()->like('LOWER(u.email)',':value'),
            $qb->expr()->like('LOWER(c.content)',':value')
        ))
            ->setParameter('value', '%'.$value.'%')
            ->orderBy('c.datetime', $order)
            ->setFirstResult($offset)
            ->setMaxResults($limit);

        $result = $qb->getQuery()->getResult();

        $countQb = $this->createQueryBuilder('c');
        $countQb->select('COUNT(c.id)')
            ->join('c.user', 'u')
            ->where($countQb->expr()->orX(
                $qb->expr()->like('LOWER(u.nickname)',':value'),
                $qb->expr()->like('LOWER(u.email)',':value'),
                $qb->expr()->like('LOWER(c.content)',':value')
            ))
            ->setParameter('value', '%'.$value.'%');

        $totalResults = $countQb->getQuery()->getSingleScalarResult();

        return $result;
    }
//    /**
//     * @return Comment[] Returns an array of Comment objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Comment
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
