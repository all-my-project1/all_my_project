<?php

namespace App\Repository;

use App\Entity\CompanyReview;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CompanyReview>
 *
 * @method CompanyReview|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompanyReview|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompanyReview[]    findAll()
 * @method CompanyReview[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyReviewRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CompanyReview::class);
    }

    public function save(CompanyReview $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(CompanyReview $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function search($value, $status, $order, $limit, $offset, &$totalResults): array
    {


        $value = strtolower($value);

        $qb = $this->createQueryBuilder('a');
        $qb->join('a.user', 'u')
            ->where($qb->expr()->orX(
                $qb->expr()->like('LOWER(u.nickname)', ':value'),
                //$qb->expr()->like('LOWER(u.email)', ':value'),
                $qb->expr()->like('LOWER(a.content)', ':value')
            ))
            ->setParameter('value', '%' . $value . '%');
        if (str_starts_with($order, 'r')) {
            $order = substr($order, 1);
            $qb->orderBy('a.rating', $order);
        }
        else
            $qb->orderBy('a.datetime', $order);
            $qb->setFirstResult($offset)
            ->setMaxResults($limit);

        if ($status !== null) {
            $qb->andWhere($qb->expr()->eq('a.home', ':status'))
                ->setParameter('status', $status);
        }

        $result = $qb->getQuery()->getResult();

        $countQb = $this->createQueryBuilder('a');
        $countQb->select('COUNT(a.id)')
            ->join('a.user', 'u')
            ->where($countQb->expr()->orX(
                $qb->expr()->like('LOWER(u.nickname)', ':value'),
                //$qb->expr()->like('LOWER(u.email)', ':value'),
                $qb->expr()->like('LOWER(a.content)', ':value')
            ))
            ->setParameter('value', '%' . $value . '%');

        if ($status !== null) {
            $countQb->andWhere($countQb->expr()->eq('a.home', ':status'))
                ->setParameter('status', $status);
        }

        $totalResults = $countQb->getQuery()->getSingleScalarResult();

        return $result;
    }
//    /**
//     * @return CompanyReview[] Returns an array of CompanyReview objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?CompanyReview
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
