<?php

namespace App\Controller;

use App\Entity\ContactUs;
use App\Form\ContactUsType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactUsController extends AbstractController
{
    public function __construct(private ManagerRegistry $doctrine) {}
    #[Route('/contact', name: 'app_contact_us')]
    public function index(Request $request)
    {
        $contact = new ContactUs();

        $form =$this->createForm(ContactUsType::class,$contact);

        $form ->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {

            $now = new \DateTime('@' . strtotime('now'));
            $now->setTimezone(new \DateTimeZone('Europe/Kiev'));
            $contact->setDatetime($now);
            $phoneNumber = $contact->getPhone(); // phone number in international format
            $cleanedNumber = "+" . preg_replace("/[^0-9]/", "", $phoneNumber);
            $contact->setPhone($cleanedNumber);
            $em =  $this->doctrine->getManager();
            $em->persist($contact);
            $em->flush();
            return new RedirectResponse($this->generateUrl('Main.Index'));
        }

        return $this->render(view: 'contact_us/index.html.twig', parameters: [
            'form'=>$form,
        ]);
    }
}
