<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use App\Form\Type\CommentsFormType;
use App\Repository\ArticleRepository;
use App\Repository\CommentRepository;
use App\Repository\UserRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/article', name: 'article.')]
class ArticleController extends AbstractController
{

    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(Request $request, ArticleRepository $articleRepository): Response
    {
        $sortBy = $request->query->get('article-sort-by');

        if ($sortBy == 'asc-title') {
            $articles = $articleRepository->findBy([], ['title' => 'asc']);
        } else if ($sortBy == 'desc-title') {
            $articles = $articleRepository->findBy([], ['title' => 'desc']);
        } else if ($sortBy == 'asc-date') {
            $articles = $articleRepository->findBy([], ['date' => 'asc']);
        } else if ($sortBy == 'desc-date') {
            $articles = $articleRepository->findBy([], ['date' => 'desc']);
        } else {
            $articles = $articleRepository->findAll();
        }

        return $this->render('article/index.html.twig', [
            'articles' => $articles
        ]);
    }

    #[Route('/{id}', name: 'view')]
    public function view(Request $request, ArticleRepository $articleRepository, CommentRepository $commentRepository, ManagerRegistry $doctrine, $id): Response
    {
        $article = $articleRepository->find($id);
        $comments = $commentRepository->findBy(['article' => $article], ['datetime' => 'ASC']);

        $user = $this->getUser();

        $otherArticles = $doctrine->getRepository(Article::class)->findBy([], ["date" => "DESC"], 5);

        $now = new \DateTime('@' . strtotime('now'));
        $now->setTimezone(new \DateTimeZone('Europe/Kiev'));

        $comment = new Comment();
        $form = $this->createForm(type: CommentsFormType::class, data: $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setUser($user);
            $comment->setArticle($article);
            $comment->setDatetime($now);

            $em = $doctrine->getManager();
            $em->persist($comment);
            $em->flush();

            return $this->redirect($this->generateUrl(route: 'article.view', parameters: ['id' => $id]));
        }

//        $comment_form_answer = $commentRepository->find($id);
//        $answerTo = new Comment();
//        $form_answer = $this->createForm(type: CommentsFormType::class, data: $answerTo);
//        $form_answer->handleRequest($request);
//
//        if ($form_answer->isSubmitted() && $form_answer->isValid()) {
//            $answerTo->setUser($user);
//            $answerTo->setReplyTo($comment_form_answer);
//            $answerTo->setArticle($comment_form_answer->getArticle());
//            $answerTo->setDatetime($now);
//
//            $em = $doctrine->getManager();
//            $em->persist($answerTo);
//            $em->flush();
//
//            return $this->redirect($this->generateUrl(route: 'article.view', parameters: ['id' => $id]));
//        }

        if ($user) {
            return $this->render(view: 'article/view.html.twig', parameters: [
                'article' => $article,
                'comments' => $comments,
                'otherArticles' => $otherArticles,
                'user' => $user->getNickname(),
                'form' => $form->createView()
//                'form_answer' => $form_answer->createView()
            ]);
        } else {
            return $this->render(view: 'article/view.html.twig', parameters: [
                'article' => $article,
                'comments' => $comments,
                'otherArticles' => $otherArticles
            ]);
        }
    }

//    #[Route('/{id}/comment/create', name: 'comment.create')]
//    public function create(ManagerRegistry $doctrine, Request $request, ArticleRepository $articleRepository, $id): Response
//    {
//        $user = $this->getUser();
//        $article = $articleRepository->find($id);
//
//        $now = new \DateTime('@' . strtotime('now'));
//        $now->setTimezone(new \DateTimeZone('Europe/Kiev'));
//
//        $comment = new Comment();
//
//        $form = $this->createForm(type: CommentsFormType::class, data: $comment);
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid()) {
//            $comment->setUser($user);
//            $comment->setArticle($article);
//            $comment->setDatetime($now);
//
//            $em = $doctrine->getManager();
//            $em->persist($comment);
//            $em->flush();
//
//            return $this->redirect($this->generateUrl(route: 'article.index'));
//        }
//
//        return $this->render(view: 'article/create.html.twig', parameters: [
//            'form' => $form->createView()
//        ]);
//    }

    #[Route('/comment/answer/{id}', name: 'comment.answer')]
    public function answerTo(ManagerRegistry $doctrine, Request $request, CommentRepository $commentRepository, $id): Response
    {
        $comment = $commentRepository->find($id);
        $user = $this->getUser();

        $answerTo = new Comment();

        $now = new \DateTime('@' . strtotime('now'));
        $now->setTimezone(new \DateTimeZone('Europe/Kiev'));

        $form = $this->createForm(type: CommentsFormType::class, data: $answerTo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $answerTo->setUser($user);
            $answerTo->setReplyTo($comment);
            $answerTo->setArticle($comment->getArticle());
            $answerTo->setDatetime($now);

            $em = $doctrine->getManager();
            $em->persist($answerTo);
            $em->flush();

            return $this->redirect($this->generateUrl(route: 'article.index'));
        }

        return $this->render(view: 'article/create.html.twig', parameters: [
            'form' => $form->createView(),
            'comment' => $comment
        ]);
    }
}
