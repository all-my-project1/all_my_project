<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\CompanyReview;
use App\Entity\ContactUs;
use App\Entity\Question;
use App\Entity\Statistics;
use App\Form\AuthorizedQuestionType;
use App\Form\CompanyReviewType;
use App\Form\ContactUsType;
use App\Form\QuestionType;
use App\Form\Type\SearchFormType;
use App\Repository\UserRepository;
use App\Service\TestService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/', name: 'Main.')]
class MainController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /** HEADER SECTION */
    public function sectionHeader(): Response
    {
        return $this->render('header.html.twig', [
            'user' => 'MainController',
        ]);
    }

    /** USER VISITOR */
    #[Route('/', name: 'VisitorRegister', methods: ["POST"])]
    public function visitorRegister(ManagerRegistry $doctrine, Request $request, UserRepository $userRepository)
    {
        $rep = $doctrine->getManager()->getRepository(Statistics::class);
        $rep->AddVisitor();

        $user = $this->getUser();
        $question = new Question();
        if ($user == null) {
            $questionForm = $this->createForm(QuestionType::class, $question);
        } else {
            $questionForm = $this->createForm(AuthorizedQuestionType::class, $question);
        }
        $questionForm->handleRequest($request);
        if ($questionForm->isSubmitted() && $questionForm->isValid()) {
            $now = new \DateTime('@' . strtotime('now'));
            $now->setTimezone(new \DateTimeZone('Europe/Kiev'));
            $question->setDatetime($now);
            if ($user == null) {
                $phoneNumber = $question->getPhone(); // phone number in international format
                $cleanedNumber = "+" . preg_replace("/[^0-9]/", "", $phoneNumber);
                $question->setPhone($cleanedNumber);
            } else {
                $question->setEmail($user->getEmail());
                $question->setPhone($user->getPhoneNumber());
                $question->setNickname($user->getNickname());
            }
            $this->entityManager->persist($question);
            $this->entityManager->flush();
        }

        $contact = new ContactUs();
        $contactForm = $this->createForm(ContactUsType::class, $contact);
        $contactForm->handleRequest($request);
        if ($contactForm->isSubmitted() && $contactForm->isValid()) {

            $now = new \DateTime('@' . strtotime('now'));
            $now->setTimezone(new \DateTimeZone('Europe/Kiev'));
            $contact->setDatetime($now);
            $phoneNumber = $contact->getPhone(); // phone number in international format
            $cleanedNumber = "+" . preg_replace("/[^0-9]/", "", $phoneNumber);
            $contact->setPhone($cleanedNumber);
            $this->entityManager->persist($contact);
            $this->entityManager->flush();
            return new RedirectResponse($this->generateUrl('Main.Index'));
        }

        if ($user != null)
            if ($user->getCompanyReview() != null) {
                $review = $user->getCompanyReview();
            } else {
                $review = new CompanyReview();
            }
        $reviewForm = $this->createForm(CompanyReviewType::class, $review);
        $reviewForm->handleRequest($request);
        if ($reviewForm->isSubmitted() && $reviewForm->isValid()) {

            $now = new \DateTime('@' . strtotime('now'));
            $now->setTimezone(new \DateTimeZone('Europe/Kiev'));
            $user = $this->getUser();
            $review->setUser($user);
            $review->setHome(false);
            $review->setDatetime($now);
            $this->entityManager->persist($review);
            $this->entityManager->flush();
            return new RedirectResponse($this->generateUrl('Main.Index'));
        }


        return new RedirectResponse($this->generateUrl('Main.Index'));
    }


    /** ROUTES */

    #[Route('/', name: 'Index', methods: ["GET"])]
    public function index(ManagerRegistry $doctrine): Response
    {
        $data = $doctrine->getManager()->getRepository(Statistics::class)->getInstance();
        $articles = $doctrine->getRepository(Article::class)->findBy([], ["date" => "DESC"], 3);
        $reviews = $doctrine->getRepository(CompanyReview::class)->findBy(["home" => "1"], ["rating" => "DESC"]);
        ///
        $user = $this->getUser();
        $question = new Question();
        if ($user == null) {
            $questionForm = $this->createForm(QuestionType::class, $question);
        } else {
            $questionForm = $this->createForm(AuthorizedQuestionType::class, $question);
        }
        ///
        $contact = new ContactUs();
        $contactForm = $this->createForm(ContactUsType::class, $contact);
        ///
        $review = new CompanyReview();
        $reviewForm = $this->createForm(CompanyReviewType::class, $review);

        return $this->render('main/index.html.twig', [
            'questionForm' => $questionForm,
            'contactForm' => $contactForm,
            'reviewForm' => $reviewForm,
            'statistics' => $data,
            'articles' => $articles,
            'reviews' => $reviews,
            'isLogged' =>  $this->getUser() instanceof \Symfony\Component\Security\Core\User\UserInterface,
        ]);
    }


    #[Route('/about', name: 'About', methods: ["GET"])]
    public function about(TestService $service): Response
    {
        $service->SomeFunc();
        return $this->render('main/about.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }


    #[Route('/search', name: 'SearchHandler', methods: ["GET"])]
    public function searchHandler(ManagerRegistry $doctrine, Request $request): Response
    {
        $pageSize = 2; //amount of articles per page

        $query = $request->query->get('q');
        $page = intval($request->query->get('p'));
        if ($page <= 1)
            $page = 1;

        //$articles = $doctrine->getRepository(Article::class)->findBy(['title' => $query]);
        $articles = $doctrine->getRepository(Article::class)->search($query, $pageSize, $page);

        return $this->render('main/search.html.twig', [
            'articles' => $articles,
            'search' => $query,
            'page' => $page,
            'pageSize' => $pageSize
        ]);
    }


    //TODO: Obsolete
    public function searchBar(): Response
    {
        $form = $this->createForm(SearchFormType::class);

        return $this->render('parts/forms/searchBar.html.twig', [
            'searchBar' => $form->createView(),
        ]);
    }

}
