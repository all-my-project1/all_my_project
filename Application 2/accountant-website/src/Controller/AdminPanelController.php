<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use App\Entity\CompanyReview;
use App\Entity\ContactUs;
use App\Entity\Question;
use App\Entity\Statistics;

use App\Entity\UsageHistory;
use App\Form\ArticleFormType;
use App\Form\Type\CommentsFormType;
use App\Form\Type\StatisticsFormType;
use App\Form\UserFormType;
use App\Repository\ArticleRepository;
use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\CompanyReviewType;
use App\Form\ContactUsEditType;
use App\Repository\CompanyReviewRepository;
use App\Repository\ContactUsRepository;

use App\Repository\QuestionRepository;
use App\Repository\UserRepository;
use DateTime;

use Doctrine\Persistence\ManagerRegistry;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

#[Route('/admin', name: 'Admin.')]
class AdminPanelController extends AbstractController
{
    private $em;

    //private $chartBuilder;
    public function __construct(EntityManagerInterface $em,)
    {
        $this->em = $em;

    }


    #[Route('/', name: 'Index')]
    public function index(): Response
    {
        return $this->render('admin_panel/index.html.twig', [
            'controller_name' => 'AdminPanelController',
        ]);
    }


    /** STATISTICS */

    #[Route('/statistics', name: 'StatisticsGet', methods: ["GET"])]
    public function statisticsGet(ManagerRegistry $doctrine): Response
    {
        $stats = $doctrine->getRepository(Statistics::class)->getInstance();
        $form = $this->createForm(StatisticsFormType::class);

        $comments = count($doctrine->getManager()->getRepository(Comment::class)->findAll());
        $reviews = count($doctrine->getManager()->getRepository(CompanyReview::class)->findAll());

        return $this->statisticsRender($form, $stats, $comments, $reviews);
    }


    #[Route('/statistics', name: 'StatisticsPost', methods: ["POST"])]
    public function statisticsPost(ManagerRegistry $doctrine, Request $request): Response
    {
        $form = $this->createForm(StatisticsFormType::class);
        $form->handleRequest($request);

        $rep = $doctrine->getManager()->getRepository(Statistics::class);
        $stats = $rep->getInstance();

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $rep->update($data['clients'], $data['clientsTotal']);
            //...
        }

        $comments = count($doctrine->getManager()->getRepository(Comment::class)->findAll());
        $reviews = count($doctrine->getManager()->getRepository(CompanyReview::class)->findAll());

        return $this->statisticsRender($form, $stats, $comments, $reviews);
    }

    private function statisticsRender(FormInterface $form, Statistics $stats, int $commentsCount, int $reviewsCount): Response
    {
        //$this->em->getRepository(UsageHistory::class)->addData(5);

        //data chart
        //$chart = $this->chartBuilder->createChart(Chart::TYPE_LINE);
        $chartData = $this->em->getRepository(UsageHistory::class)->last7Days();

        $labels = [];
        $views = [];
        foreach ($chartData as $item) {
            $labels[] = $item->getDate()->format("Y-m-d");
            $views[] = $item->getViews();
        }

        /*
                $chart->setData([
                    'labels' => ["a", "b"],
                    'datasets' => [
                        [
                            'label' => 'Views',
                            'backgroundColor' => 'rgb(255, 99, 132)',
                            'borderColor' => 'rgb(255, 99, 132)',
                            'data' => [1, 2],
                        ],
                    ],
                ]);

                $chart->setOptions([
                    'scales' => [
                        'y' => [
                            'suggestedMin' => 0,
                            'suggestedMax' => 100,
                        ],
                    ],
                ]);
        */

        return $this->render('admin_panel/statistics/statistics.html.twig', [
            'form' => $form->createView(),
            'statistics' => $stats,
            'comments' => $commentsCount,
            'reviews' => $reviewsCount,
            'chart' => [
                'labels' => $labels,

                'data' => [
                    'views' => $views
                ]
            ]
        ]);
    }

    /** COMMENTS */

    #[Route('/comments', name: 'commentsGet', methods: ['GET'])]
    public function commentGet(CommentRepository $commentRepository, Request $request, EntityManagerInterface $em): Response
    {
        $search = $request->query->get('search');

        $sortBy = $request->query->get('comment-sort', 'DESC');
        $page = $request->query->get('page', 1);
        $limit = $request->query->get('limit', 5);
        $offset = ($page - 1) * $limit;
        $comments = $commentRepository->search($search, $sortBy, $limit, $offset, $totalItems);
        $Pages = ceil($totalItems / $limit);
        return $this->render('admin_panel/comments/index.html.twig', [
            'route' => '',
            'comments' => $comments,
            'search' => $search,
            'sortBy' => $sortBy,
            'Pages' => $Pages,
            'totalItems' => $totalItems,
            'viewedItems' => count($comments),
            'page' => $page,
            'limit' => $limit,
        ]);
    }

    #[Route('/comments/{id}', name: 'comments.id')]
    public function commentById(CommentRepository $commentRepository, Request $request, EntityManagerInterface $em, $id)
    {
        $search = $request->query->get('search');
        $sortBy = $request->query->get('comment-sort', 'DESC');
        $page = $request->query->get('page', 1);
        $limit = $request->query->get('limit', 5);
        $offset = ($page - 1) * $limit;
        $comments = [$commentRepository->find($id)];
        $totalItems = 1;
        $Pages = ceil($totalItems / $limit);
        return $this->render('admin_panel/comments/index.html.twig', [
            'route' => 'id',
            'comments' => $comments,
            'search' => $search,
            'sortBy' => $sortBy,
            'Pages' => $Pages,
            'totalItems' => $totalItems,
            'viewedItems' => count($comments),
            'page' => $page,
            'limit' => $limit,
        ]);
    }

    #[Route('/comments/answer/{id}', name: 'comments.answerTo')]
    public function answerTo(ManagerRegistry $doctrine, Request $request, CommentRepository $commentRepository, $id): Response
    {
        $comment = $commentRepository->find($id);
        $user = $this->getUser();

        $answerTo = new Comment();

        $now = new \DateTime('@' . strtotime('now'));
        $now->setTimezone(new \DateTimeZone('Europe/Kiev'));

        $form = $this->createForm(type: CommentsFormType::class, data: $answerTo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $answerTo->setUser($user);
            $answerTo->setReplyTo($comment);
            $answerTo->setArticle($comment->getArticle());
            $answerTo->setDatetime($now);

            $em = $doctrine->getManager();
            $em->persist($answerTo);
            $em->flush();

            $this->addFlash(type: 'success', message: 'Прокоментовано');

            return $this->redirect($this->generateUrl(route: 'Admin.commentsGet'));
        }

        return $this->render(view: 'admin_panel/comments/answerTo.html.twig', parameters: [
            'form' => $form->createView(),
            'comment' => $comment
        ]);
    }

    #[Route('/comments/delete/{id}', name: 'comments.delete')]
    public function remove(ManagerRegistry $doctrine, CommentRepository $commentRepository, $id)
    {
        $comment = $commentRepository->find($id);
        $this->deleteCommentAndReplies($comment, $doctrine);

        $this->addFlash(type: 'success', message: 'Коментар видалено');

        return $this->redirect($this->generateUrl(route: 'Admin.commentsGet'));
    }

    #[Route('/comments/edit/{id}', name: 'comments.edit')]
    public function edit(ManagerRegistry $doctrine, Request $request, CommentRepository $commentRepository, $id)
    {
        $commentOne = $commentRepository->find($id);

        $form = $this->createForm(type: CommentsFormType::class, data: $commentOne);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $em = $doctrine->getManager();
            $em->flush();

            $this->addFlash(type: 'success', message: 'Коментар оновлено');

            return $this->redirect($this->generateUrl(route: 'Admin.commentsGet'));
        }

        return $this->render(view: 'admin_panel/comments/edit.html.twig', parameters: [
            'form' => $form->createView()
        ]);
    }

    private function deleteCommentAndReplies(Comment $comment, ManagerRegistry $doctrine)
    {
        $entityManager = $doctrine->getManager();

        foreach ($comment->getReplies() as $reply) {
            $this->deleteCommentAndReplies($reply, $doctrine);
        }

        $entityManager->remove($comment);
        $entityManager->flush();
    }


    /** REVIEWS */

    #[Route('/reviews', name: 'reviewsGet', methods: ['GET'])]
    public function reviewGet(CompanyReviewRepository $companyReviewRepository, Request $request, EntityManagerInterface $em): Response
    {
        $searchByNickname = $request->query->get('searchByName');
        $sortBy = $request->query->get('review-sort');
        $search = $request->query->get('searchByName');
        $sortBy = $request->query->get('review-sort', 'DESC');
        $reviewHome = $request->query->get('review-home');
        $page = $request->query->get('page', 1); // Get the current page, default to 1
        $limit = $request->query->get('limit', 5); // Get the limit, default to 10
        $offset = ($page - 1) * $limit;
        $reviewHome = ($reviewHome !== "1" && $reviewHome !== "0") ? null : $reviewHome;
        $reviews = $companyReviewRepository->search($search, $reviewHome, $sortBy, $limit, $offset, $totalItems);

        $Pages = ceil($totalItems / $limit); // Calculate the total number of pages
        return $this->render('admin_panel/reviews/index.html.twig', [
            'reviews' => $reviews,
            'searchByName' => $search,
            'sortBy' => $sortBy,
            'reviewHome' => $reviewHome,
            'Pages' => $Pages,
            'totalItems' => $totalItems,
            'viewedItems' => count($reviews),
            'page' => $page,
            'limit' => $limit,
        ]);
    }


    #[Route('/reviews/delete/{id}', name: 'reviews.delete')]
    public function reviewRemove(ManagerRegistry $doctrine, CompanyReview $review)
    {
        $em = $doctrine->getManager();

        $em->remove($review);
        $em->flush();
        $this->addFlash(type: 'success', message: 'Відгук видалено.');
        return $this->redirect($this->generateUrl(route: 'Admin.reviewsGet'));
    }

    #[Route('/reviews/main/{id}', name: 'reviews.main')]
    public function reviewMain(ManagerRegistry $doctrine, CompanyReview $review)
    {
        $review->setHome(!$review->getHome());
        $em = $doctrine->getManager();
        $em->persist($review);
        $em->flush();
        $this->addFlash(type: 'success', message: 'Відгук змінено');
        return $this->redirect($this->generateUrl(route: 'Admin.reviewsGet'));
    }

    #[Route('/reviews/edit/{id}', name: 'reviews.edit')]
    public function reviewEdit(ManagerRegistry $doctrine, CompanyReview $review, Request $request, CompanyReviewRepository $reviewRepository)
    {
        $reviewOne = $reviewRepository->find($review);

        $form = $this->createForm(type: CompanyReviewType::class, data: $reviewOne);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $em = $doctrine->getManager();
            $em->flush();

            $this->addFlash(type: 'success', message: 'Відгук оновлено.');

            return $this->redirect($this->generateUrl(route: 'Admin.reviewsGet'));
        }

        return $this->render(view: 'admin_panel/reviews/edit.html.twig', parameters: [
            'form' => $form->createView()
        ]);
    }


    /** contacts */

    #[Route('/contacts', name: 'contactsGet', methods: ['GET'])]
    public function contactsGet(ContactUsRepository $contactUsRepository, Request $request, EntityManagerInterface $em): Response
    {

        $search = $request->query->get('search');
        $sortBy = $request->query->get('contact-sort', 'DESC');
        $page = $request->query->get('page', 1); // Get the current page, default to 1
        $limit = $request->query->get('limit', 5); // Get the limit, default to 10
        $offset = ($page - 1) * $limit; // Calculate the offset

//        $date = ['datetime' => 'desc'];
//        if ($sortBy == 'ASC') {
//            $date = ['datetime' => 'asc'];
//        }
//        $searchBy = [];
//        if ($search) {
//            // Check if the search term is an email address
//            if (filter_var($search, FILTER_VALIDATE_EMAIL)) {
//                $searchBy = ['email' => $search];
//            } else {
//                $phoneUtil = PhoneNumberUtil::getInstance();
//                try {
//                    $phoneNumber = $phoneUtil->parse($search, 'UA');
//                    if ($phoneUtil->isValidNumber($phoneNumber)) {
//                        $formattedPhoneNumber = $phoneUtil->format($phoneNumber, PhoneNumberFormat::E164);
//                        $searchBy = ['phone' => $formattedPhoneNumber];
//                    } else {
//                        $searchBy = ['name' => $search];
//                    }
//                } catch (\libphonenumber\NumberParseException $e) {
//                    $searchBy = ['name' => $search];
//                }
//            }
//        }
//
//          $contacts = $contactUsRepository->findBy($searchBy, $date, $limit, $offset);
//          $totalItems = $contactUsRepository->count($searchBy); // Count the total number of items
        $contacts = $contactUsRepository->search($search, $sortBy, $limit, $offset, $totalItems);


        $Pages = ceil($totalItems / $limit); // Calculate the total number of pages
        return $this->render('admin_panel/contacts/index.html.twig', [
            'contacts' => $contacts,
            'search' => $search,
            'sortBy' => $sortBy,
            'Pages' => $Pages,
            'totalItems' => $totalItems,
            'viewedItems' => count($contacts),
            'page' => $page,
            'limit' => $limit,
        ]);
    }

    #[Route('/contacts/delete/{id}', name: 'contacts.delete')]
    public function contactRemove(ManagerRegistry $doctrine, ContactUs $contact)
    {
        $em = $doctrine->getManager();

        $em->remove($contact);
        $em->flush();
        $this->addFlash(type: 'success', message: 'Контакт видалено.');
        return $this->redirect($this->generateUrl(route: 'Admin.contactsGet'));
    }

    #[Route('/contacts/edit/{id}', name: 'contacts.edit')]
    public function contactEdit(ManagerRegistry $doctrine, ContactUs $contactUs, Request $request, ContactUsRepository $contactUsRepository)
    {
        $reviewOne = $contactUsRepository->find($contactUs);
        $form = $this->createForm(type: ContactUsEditType::class, data: $reviewOne);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $em = $doctrine->getManager();
            $em->flush();

            $this->addFlash(type: 'success', message: 'Відгук оновлено.');

            return $this->redirect($this->generateUrl(route: 'Admin.contactsGet'));
        }

        return $this->render(view: 'admin_panel/contacts/edit.html.twig', parameters: [
            'form' => $form->createView()
        ]);
    }

    //Question
    #[Route('/questions', name: 'questionGet', methods: ['GET'])]
    public function questionGet(QuestionRepository $questionRepository, Request $request): Response
    {

        $search = $request->query->get('search');
        $sortBy = $request->query->get('question-sort');
        $page = $request->query->get('page', 1); // Get the current page, default to 1
        $limit = $request->query->get('limit', 5); // Get the limit, default to 10
        $offset = ($page - 1) * $limit; // Calculate the offset

//        $date = ['datetime' => 'desc'];
//        if ($sortBy == 'ASC') {
//            $date = ['datetime' => 'asc'];
//        }
//        $searchBy = [];
//        if ($search) {
//            // Check if the search term is an email address
//            if (filter_var($search, FILTER_VALIDATE_EMAIL)) {
//                $searchBy = ['email' => $search];
//            } else {
//                $phoneUtil = PhoneNumberUtil::getInstance();
//                try {
//                    $phoneNumber = $phoneUtil->parse($search, 'UA');
//                    if ($phoneUtil->isValidNumber($phoneNumber)) {
//                        $formattedPhoneNumber = $phoneUtil->format($phoneNumber, PhoneNumberFormat::E164);
//                        $searchBy = ['phone' => $formattedPhoneNumber];
//                    } else {
//                        $searchBy = ['nickname' => $search];
//                    }
//                } catch (\libphonenumber\NumberParseException $e) {
//                    $searchBy = ['nickname' => $search];
//                }
//            }
//        }
//
//        $questions = $questionRepository->findBy($searchBy, $date, $limit, $offset);

        $questions = $questionRepository->search($search, $sortBy, $limit, $offset, $totalItems);

        $Pages = ceil($totalItems / $limit); // Calculate the total number of pages
        return $this->render('admin_panel/question/index.html.twig', [
            'questions' => $questions,
            'search' => $search,
            'sortBy' => $sortBy,
            'Pages' => $Pages,
            'totalItems' => $totalItems,
            'viewedItems' => count($questions),
            'page' => $page,
            'limit' => $limit,
        ]);
    }

    /** USERS */
    #[Route('/users', name: 'users', methods: ['GET'])]
    public function usersShow(Request $request, UserRepository $userRepository): Response
    {
        $search = $request->query->get('search');
        $page = $request->query->get('page', 1); // Get the current page, default to 1
        $limit = $request->query->get('limit', 5); // Get the limit, default to 10
        $offset = ($page - 1) * $limit; // Calculate the offset


//        $searchBy = [];
//        if ($search) {
//            if (filter_var($search, FILTER_VALIDATE_EMAIL)) {
//                $searchBy = ['email' => $search];
//            } else {
//                $phoneUtil = PhoneNumberUtil::getInstance();
//                try {
//                    $phoneNumber = $phoneUtil->parse($search, 'UA');
//                    if ($phoneUtil->isValidNumber($phoneNumber)) {
//                        $formattedPhoneNumber = $phoneUtil->format($phoneNumber, PhoneNumberFormat::E164);
//                        $searchBy = ['phone' => $formattedPhoneNumber];
//                    } else {
//                        $searchBy = ['nickname' => $search];
//                    }
//                } catch (\libphonenumber\NumberParseException $e) {
//                    $searchBy = ['nickname' => $search];
//                }
//            }
//        }
//
//
//        $users = $userRepository->findBy($searchBy, [], $limit, $offset);
//        $totalItems = $userRepository->count($searchBy); // Count the total number of items

        $users = $userRepository->search($search, $limit, $offset, $totalItems);

        $Pages = ceil($totalItems / $limit); // Calculate the total number of pages
        return $this->render('admin_panel/users/users.html.twig', [
            'users' => $users,
            'search' => $search,
            'Pages' => $Pages,
            'totalItems' => $totalItems,
            'viewedItems' => count($users),
            'page' => $page,
            'limit' => $limit,
        ]);
    }

    #[Route('/users/{id}', name: 'user', methods: ['GET'])]
    public function usersView(UserRepository $userRepository, $id): Response
    {
        $users = $userRepository->find($id);
        return $this->render('admin_panel/users/view.html.twig', [
            'users' => $users
        ]);
    }

    #[Route('/users/edit/{id}', name: 'edit_user')]
    public function editUser(UserRepository $userRepository, $id, Request $request): Response
    {
        $user = $userRepository->find($id);
        $form = $this->createForm(UserFormType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setNickname($form->get('nickname')->getData());
            $user->setPhoneNumber($form->get('phone_number')->getData());
            $user->setEmail($form->get('email')->getData());
            $user->setRoles($form->get('roles')->getData());

            $this->em->flush();
            return $this->redirectToRoute('Admin.users');
        }

        return $this->render('admin_panel/users/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView()
        ]);
    }


    /** ARTICLES
     * @param ArticleRepository $articleRepository
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param $totalItems
     * @return Response
     */
    #[Route('/articles', name: 'articles', methods: ['GET'])]
    public function articleList(ArticleRepository $articleRepository, Request $request, EntityManagerInterface $em): Response
    {

        $articles = $articleRepository->findBy([], ["date"=>"DESC"]);
        #dd($articles);

        $search = $request->query->get('search');
        $sortBy = $request->query->get('article-sort', 'DESC');
        $articleStatus = $request->query->get('article-status', null);
        $page = $request->query->get('page', 1); // Get the current page, default to 1
        $limit = $request->query->get('limit', 5); // Get the limit, default to 10
        $offset = ($page - 1) * $limit; // Calculate the offset


        $articleStatus = ($articleStatus !== "1" && $articleStatus !== "0") ? null : $articleStatus;
        $articles = $articleRepository->new_search($search, $articleStatus, $sortBy, $limit, $offset, $totalItems);
        $Pages = ceil($totalItems / $limit); // Calculate the total number of pages

        return $this->render('admin_panel/articles/articles.html.twig', [
            'articles' => $articles,
            'search' => $search,
            'sortBy' => $sortBy,
            'articleStatus' => $articleStatus,
            'Pages' => $Pages,
            'totalItems' => $totalItems,
            'viewedItems' => count($articles),
            'page' => $page,
            'limit' => $limit,
        ]);
    }

    #[Route('/articles/create', name: 'create_article')]
    public function createArticle(Request $request, SluggerInterface $slugger): Response
    {
        $article = new Article();
        $form = $this->createForm(ArticleFormType::class, $article);

        $form->get("date")->setData(new DateTime()); //today by default

        //$now = new \DateTime('@' . strtotime('now'));
        //$now->setTimezone(new \DateTimeZone('Europe/Kiev'));
        //$article->setDate($now);


        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $newArticle = $form->getData();
            $imagePath = $form->get('image')->getData();
            if ($imagePath) {

                $originalFilename = pathinfo($imagePath->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.' . $imagePath->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $imagePath->move(
                        $this->getParameter('kernel.project_dir') . '/public/uploads',
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
                $article->setImage($newFilename);
            }
            $this->em->persist($newArticle);
            $this->em->flush();

            return $this->redirectToRoute('Admin.articles');
        }

        return $this->render('/admin_panel/articles/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    #[Route('/articles/delete/{id}', name: 'delete_article', methods: ['GET', 'DELETE'])]
    public function deleteArticle(ArticleRepository $articleRepository, $id): Response
    {
        $article = $articleRepository->find($id);
        $this->em->remove($article);
        $this->em->flush();

        return $this->redirectToRoute('Admin.articles');
    }

    #[Route('/articles/{id}', name: 'article', methods: ['GET'])]
    public function articleView(ArticleRepository $articleRepository, $id): Response
    {
        $article = $articleRepository->find($id);
        return $this->render('admin_panel/articles/view.html.twig', [
            'article' => $article
        ]);
    }

    #[Route('/articles/edit/{id}', name: 'edit_article')]
    public function editArticle(ArticleRepository $articleRepository, $id, Request $request, SluggerInterface $slugger): Response
    {
        $article = $articleRepository->find($id);
        $form = $this->createForm(ArticleFormType::class, $article);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $imagePath = $form->get('image')->getData();
            if ($imagePath) {
                $destination = $this->getParameter('kernel.project_dir') . '/public/uploads';

                $originalFilename = pathinfo($imagePath->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.' . $imagePath->guessExtension();

                $imagePath->move(
                    $destination,
                    $newFilename
                );

                $article->setImage($newFilename);
            } else {
                $article->setTitle($form->get('title')->getData());
                $article->setContent($form->get('content')->getData());
                $article->setDate($form->get('date')->getData());
                $article->setStatus($form->get('status')->getData());
            }


            $this->em->persist($article);
            $this->em->flush();

            return $this->redirectToRoute('Admin.articles');
        }

        return $this->render('admin_panel/articles/edit.html.twig', [
            'article' => $article,
            'form' => $form->createView()
        ]);
    }

    #[Route('/questions/delete/{id}', name: 'question.delete')]
    public function questionRemove(ManagerRegistry $doctrine, Question $question)
    {
        $em = $doctrine->getManager();

        $em->remove($question);
        $em->flush();
        $this->addFlash(type: 'success', message: 'Запитання видалено.');
        return $this->redirect($this->generateUrl(route: 'Admin.questionGet'));
    }
}
