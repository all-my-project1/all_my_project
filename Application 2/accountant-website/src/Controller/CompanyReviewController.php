<?php

namespace App\Controller;

use App\Entity\CompanyReview;
use App\Form\CompanyReviewType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CompanyReviewController extends AbstractController
{
    public function __construct(private ManagerRegistry $doctrine) {}
    #[Route('/review', name: 'Review')]
    public function index(Request $request)
    {
        $user = $this->getUser();
        if($user == null)
        {
            return $this->render(view: 'error.html.twig', parameters: [
                'error'=>"Ви зарегайтесь спочатку. а!?!??!",
            ]);
        }
        if ($user->getCompanyReview() != null)
        {
            return $this->render(view: 'error.html.twig', parameters: [
                'error'=>"Ви уже блін зробили відгук, алйооо",
            ]);
        }
        else {
            $review = new CompanyReview();

            $form = $this->createForm(CompanyReviewType::class, $review);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $now = new \DateTime('@' . strtotime('now'));
                $now->setTimezone(new \DateTimeZone('Europe/Kiev'));
                $user = $this->getUser();
                $review->setUser($user);
                $review->setHome(false);
                $review->setDatetime($now);
                $em = $this->doctrine->getManager();
                $em->persist($review);
                $em->flush();
                return new RedirectResponse($this->generateUrl('Main.Index'));
            }

            return $this->render(view: 'company_review/index.html.twig', parameters: [
                'form' => $form,
            ]);
        }
    }
}
