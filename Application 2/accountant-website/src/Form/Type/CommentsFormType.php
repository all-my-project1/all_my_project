<?php

namespace App\Form\Type;

use App\Entity\Comment;
use Doctrine\DBAL\Types\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommentsFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('content', type: TextareaType::class, options: [
                'label' => false,
                'attr' => [
                    'class' => 'form-control-textarea mt-4',
                    'placeholder' => 'Введіть текст коментаря ...',
                    'rows' => '5'
                ]
            ])
            ->add('save', type: SubmitType::class, options: [
                'label' => 'Залишити коментар',
                'attr' => [
                    'class' => 'btn btn-outline-success'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Comment::class,
        ]);
    }
}
