<?php

namespace App\Form;

use App\Entity\Article;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'attr'=> array(
                    'class'=>'form-control p-2',
                    'placeholder'=>'Заголовок новини'
                ),
                'label'=>'Заголовок'
            ])
            ->add('image', FileType::class, array(
                'required'=> false,
                'mapped' => false,
                'label'=>'Зображення'
            ))
            ->add('content', TextareaType::class, [
                'attr'=> array(
                    'class'=>'form-control p-2 h-100',
                    'placeholder'=>'Контент'
                ),
                'label'=>'Вміст новини'
            ])
            ->add('status', CheckboxType::class,[
                'label'=>'Опубліковано',
                'required' => false
            ])
            ->add('date',DateType::class, [
                'label'=>'Дата публікації',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
