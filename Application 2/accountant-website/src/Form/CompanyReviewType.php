<?php

namespace App\Form;

use App\Entity\CompanyReview;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class CompanyReviewType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('rating', TextType::class,[
                'label' => false,
                'attr' => [
                    'class' => 's',
                    'step' => 1,
                    ],
            ])
            ->add('content',null, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'Коментар ...',
                    'style' => 'height: 10vh;'
                ]
            ])
            ->add('add', SubmitType::class, [
                'label' => 'Додати відгук',
                'attr' => [
                    'class' => 'radius30 btn-outline-success',
                    'style' => 'width: 100%;'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CompanyReview::class,
        ]);
    }
}
