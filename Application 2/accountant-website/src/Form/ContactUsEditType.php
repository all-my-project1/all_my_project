<?php

namespace App\Form;

use App\Entity\ContactUs;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactUsEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name',null,['label' => 'Прізвище Ім\'я'
            ])
            ->add('email',null,['label' => 'Електронна пошта'
            ])
            ->add('phone', null, [
                'label' => 'Номер телефону',
            ])
            ->add('add', SubmitType::class, [
                'label' => 'Редагувати контакт',
                'attr' => [
                    'class' => 'btn-outline-success'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ContactUs::class,
        ]);
    }
}
