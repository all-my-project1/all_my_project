<?php

namespace App\Form;

use App\Entity\ContactUs;
use libphonenumber\PhoneNumberFormat;
use Misd\PhoneNumberBundle\Form\Type\PhoneNumberType;
use Misd\PhoneNumberBundle\Validator\Constraints\PhoneNumber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactUsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', null, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'Ім\'я'
                ]
            ])
            ->add('email', null, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'Ел. пошта'
                ]
            ])
            ->add('phone', PhoneNumberType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'Номер телефону'
                ],
                'format' => PhoneNumberFormat::E164,
                'default_region' => 'UA',
                'constraints' => [
                    new PhoneNumber([
                        'message' => 'Це значення не є дійсним номером телефону'
                    ])
                ]
            ])
            ->add('add', SubmitType::class, [
                'label' => 'Залишити контакт',
                'attr' => [
                    'class' => 'radius30 btn-outline-success',
                    'style' => 'width: 100%;'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ContactUs::class,
        ]);
    }
}
