<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class,[
                'attr'=> array(
                    'class'=>'form-control p-2'
                ),
                'label'=>'Електронна пошта користувача'
            ])
            ->add('nickname', TextType::class,[
                'attr'=> array(
                    'class'=>'form-control p-2'
                ),
                'label'=>'Нікнейм'
            ])
            ->add('phone_number', TextType::class,[
                'attr'=> array(
                    'class'=>'form-control p-2'
                ),
                'label'=>'Номер телефону'
            ])
            ->add('roles', CollectionType::class);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
