<?php

namespace App\Form;

use App\Entity\Question;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AuthorizedQuestionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', null, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'Тема запитання'
                ],
            ])
            ->add('content', null,[
                'label' => false,
                'attr' => [
                    'placeholder' => 'Запитання ...'
                ],
            ])
            ->add('add', SubmitType::class, [
                'label' => 'Залишити питання',
                'attr' => [
                    'class' => 'radius30 btn-outline-success',
                    'style' => 'width: 100%;'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Question::class,
        ]);
    }
}
