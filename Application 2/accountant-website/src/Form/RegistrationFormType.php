<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class)
            ->add('nickname', TextType::class/*, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter nickname',
                    ]),
                    new Length([
                        'min' => 3,
                        'minMessage' => 'Your nickname should be at least {{ limit }} characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 60,
                    ]),
                ],
            ]*/)
            ->add('phone_number', TextType::class/*, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter phone',
                    ]),
                    new Length([
                        'min' => 9,
                        'minMessage' => 'Your phone should be at least {{ limit }} characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 17,
                    ]),
                ],
            ]*/)
            ->add('agreeTerms', CheckboxType::class, [
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'Ви повинні погодитися з нашими умовами.',
                    ]),
                ],
            ])
            ->add('plainPassword', PasswordType::class,[
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'attr' => ['autocomplete' => 'new-password'],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Будь ласка, введіть пароль.',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Ваш пароль має містити принаймні {{ limit }} символів.',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
