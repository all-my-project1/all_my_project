<?php

namespace App\EventSubscriber;
use App\Entity\Statistics;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class VisitorSubscriber //implements EventSubscriberInterface
{
    private $doctrine;
    private const COOKIE_NAME = "Visitor";

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
    }


    public static function getSubscribedEvents()
    {

        /*

        return [
            KernelEvents::RESPONSE => [
                ['CheckVisitor'],
            ],
        ];

        return [
            //RequestEvent::class => 'CheckVisitorRequest',
           ResponseEvent::class => 'CheckVisitor',
        ];
        */
    }

    public function CheckVisitor(ResponseEvent $event)
    {
        $request = $event->getRequest();
        if($request->cookies->get(self::COOKIE_NAME))
        {
            //user is already visited site today
            dump("Cookie found!");
        }
        else
        {
            //user is new today, add him
            $rep = $this->doctrine->getManager()->getRepository(Statistics::class);
            $rep->AddVisitor();

            //set cookie
            $response = $event->getResponse();
            $cookie = new Cookie(self::COOKIE_NAME, 1, 60,
                "/",                                     // Path
                "localhost",                             // Domain
                $request->getScheme() === 'https',       // Secure
                false,                                   // HttpOnly
                true,                                    // Raw
                'Strict' ); //24 * 3600

            $res = new JsonResponse();
            $res->headers->setCookie($cookie);
            $response->headers->setCookie($cookie);

            //dump("New user!");
        }


    }

}