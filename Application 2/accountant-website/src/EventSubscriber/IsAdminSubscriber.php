<?php

namespace App\EventSubscriber;

use App\Controller\AdminPanelController;
use Symfony\Bridge\Doctrine\ManagerRegistry;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class IsAdminSubscriber //implements EventSubscriberInterface
{
    private $security;
    private $em;

    public function __construct(Security $security, ManagerRegistry $doctrine)
    {
        $this->security = $security;
        $this->em = $doctrine->getManager();
    }

    public function onKernelController(ControllerEvent $event)
    {
        $controller = $event->getController();
        $action = 'index';
        $hasAccess = false;

        // when a controller class defines multiple action methods, the controller
        // is returned as [$controllerInstance, 'methodName']
        if (is_array($controller)) {
            $action = $controller[1];
            $controller = $controller[0];
        }

        if ($controller instanceof AdminPanelController)
        {
            /*
            if ($this->security->isGranted('ROLE_USER')) //if logged in
            {
                $user = $this->security->getUser();
                $officer = $this->em->getRepository(Officer::class)->GetOfficer($user);

            }
                */
        }
    }



    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
        ];
    }
}