document.addEventListener('DOMContentLoaded', function() {
    let copyLinks = document.querySelectorAll('.CopyAble');
    let currentPopover = null;

    for (let i = 0; i < copyLinks.length; i++) {
        copyLinks[i].addEventListener('click', function(event) {
            event.preventDefault();
            let text = this.textContent;
            navigator.clipboard.writeText(text);

            // Hide current popover, if any
            if (currentPopover) {
                currentPopover.hide();
                currentPopover = null;
            }

            // Show new popover
            let popover = new bootstrap.Popover(this, {
                content: 'Скопійовано!',
                placement: 'top',
                trigger: 'manual'
            });
            popover.show();
            currentPopover = popover;
            setTimeout(() => {
                popover.hide();
                currentPopover = null;
            }, 2000);
        });
    }
});
