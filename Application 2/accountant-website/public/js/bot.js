document.cookie = "cookiename=cookievalue; expires=Thu, 18 Dec 2023 12:00:00 UTC; path=/; SameSite=None; Secure";
const questions = [
    {
        question: "Привіт. Я ваш гід. Можливо у вас з'явились запитання?",
        answers: [
            {text: "Так", next: 1},
            {text: "Ні", redirect: "/"},
        ]
    },
    {
        question: "Оберіть тему яка вас цікавить!",
        answers: [
            {text: "Найчастіші запитання до нас", next: 2},
            {text: "Тлумачення визначень", next: 6},
            {text: "Технічна підтримка", redirect: "/"},
            {text: "Правила сайту", next: 12},
        ]
    },
    {
        question: "Оберіть питання яке вас зацікавило!",
        answers: [
            {text: "Чи допомагаєте відкрити ФОП з нуля?", next: 3},
            {text: "Чи відновлюєте бухгалтерський облік минулих років ?", next: 4},
            {text: "Чи допомагаєте в оформленні пенсії ?", next: 5},
        ]
    },
    {
        question: "Так, це одна з основних наших послуг.",
        answers: []
    },
    {
        question: "Так, у разі втрати документів, ми відновлюємо облік.",
        answers: []
    },
    {
        question: "Так, бо нещодавно з'явилася можливість отримати її онлайн.",
        answers: []
    },
    {
        question: "Виберіть визначення значення якого хотіли дізнатись.",
        answers: [
            {text: "Бухга́лтер", next: 7},
            {text: "Аутсо́рсинг", next: 8},
            {text: "Аванс", next: 9},
            {text: "Оренда", next: 10},
            {text: "Дебітори", next: 11},
        ]
    },
    {
        question: "Бухга́лтер - це кваліфікований фахівець спеціальної сфери знань, якийзаймається веденням та контролем обліку господарської діяльності, складанням та поданням звітності.",
        answers: []
    },
    {
        question: "Аутсо́рсинг - це передача компанією частини її завдань або процесів стороннім виконавцям на умовах субпідряду.",
        answers: []
    },
    {
        question: "Аванс - це кошти, що видаються заздалегідь рахунок покриття майбутніх витрат чи платежів.",
        answers: []
    },
    {
        question: "Оренда - це термінове відшкодувальне володіння та користування майном іншої організації.",
        answers: []
    },
    {
        question: "Дебітори - це юридичні та фізичні особи - боржники організації.",
        answers: []
    },
    {
        question: "Поважайте себе та наших працівників.",
        answers: []
    },
];

let currentQuestion = 0;
let history = [];

const showQuestion = () => {
    const question = questions[currentQuestion];
    document.querySelector("#question").textContent = question.question;
    const answers = question.answers;
    const answersList = document.querySelector("#answers");
    answersList.innerHTML = "";
    answers.forEach(answer => {
        const button = document.createElement("button");
        button.textContent = answer.text;
        button.classList.add("btn", "btn-outline-success", "mt-3", "m-1");
        button.addEventListener("click", () => {
            if (answer.redirect != null)
                window.location.href = answer.redirect;
            else {
                history.push(currentQuestion);
                currentQuestion = answer.next;
                showQuestion();
            }
        });
        answersList.appendChild(button);
    });
    const previousButton = document.createElement("button");
    previousButton.textContent = "Повернутись";
    previousButton.classList.add("btn", "btn-outline-success", "mt-3", "m-1");
    previousButton.addEventListener("click", () => {
        if (history.length > 0) {
            currentQuestion = history.pop();
            showQuestion();
        }
    });
    answersList.appendChild(previousButton);
    const startOverButton = document.createElement("button");
    startOverButton.textContent = "З початку";
    startOverButton.classList.add("btn", "btn-outline-success", "mt-3", "m-1");
    startOverButton.addEventListener("click", () => {
        history = [];
        currentQuestion = 0;
        showQuestion();
    });
    answersList.appendChild(startOverButton);
};

showQuestion();
function hide_bot() {

    var bot = document.querySelector('.bot');
    var closeBtn = document.querySelector('.close-btn');

    if (closeBtn.classList.contains('close-btn-show')) {
        closeBtn.classList.remove('close-btn-show');
        closeBtn.classList.add('close-btn-hide');
        bot.classList.remove('bot-hide');
        bot.classList.add('bot-show');
        console.log(3);
    }
    else {
        bot.classList.remove('bot-show');
        bot.classList.add('bot-hide');
        closeBtn.classList.remove('close-btn-hide');
        closeBtn.classList.add('close-btn-show');
        console.log(4);
    }

}

