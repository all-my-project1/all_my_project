$(document).ready(function(){
    $('.input-id').rating({min:0, size:'xs', displayOnly: true, showCaption: false,
        starTitles: {
            1: 'Одна зірка',
            2: 'Дві зірки',
            3: 'Три зірки',
            4: 'Чотири зірки',
            5: 'П\'ять зірок'
        }
    });
});