/** VISITOR CHECKER */
const cookieName = "Visited";

function Start(path)
{
    if(getCookie(cookieName) === undefined)
        setAsVisitor(path);
}

function sendPost(path)
{
    fetch(path, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ "data": 1 })
    })
    //.then(response => response.json())
    //.then(response => console.log(JSON.stringify(response)))
}

function setAsVisitor(path)
{
    const d = new Date();
    const h = d.getHours();
    const m = d.getMinutes();
    const s = d.getSeconds();
    const timeLeft = (24*60*60) - (h*60*60) - (m*60) - s;

    sendPost(path);
    let nextDate = new Date();
    nextDate.setSeconds(timeLeft);
    //console.log(nextDate)
    setCookie(cookieName, 1, { expires: nextDate });
}


// returns cookies with name or undefined
function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));

    // decoded
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

// Установлення кукі з іменем "name" та значенням "value"
function setCookie(name, value, options = {}) {
    options = {
        path: '/',

        // additional params
        ...options
    };

    if (options.expires instanceof Date) {
        options.expires = options.expires.toUTCString();
    }

    let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

    for (let optionKey in options) {
        updatedCookie += "; " + optionKey;
        let optionValue = options[optionKey];
        if (optionValue !== true) {
            updatedCookie += "=" + optionValue;
        }
    }

    document.cookie = updatedCookie;
}

