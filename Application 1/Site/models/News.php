<?php

namespace models;

use core\Model;
use core\Utils;

class News extends Model
{
    public function NewsComments($newsId){
        $comments = \core\Core::getInstance()->getDB()->select('comments','*',['news_id'=>$newsId],['data_time'=>'DESC']);
        $result = [];
        $userModel = new \models\Users();
        foreach ($comments as $comment){
            $user = $userModel->GetUserByID($comment['user_id']);
            $comment['name']= $user['lastname'].' '.$user['firstname'];
            $result[]=$comment;
        }
        return $result;
    }
    public function AddNewsComments($newsId,$text){
        $userModel = new \models\Users();
        $user = $userModel->GetCurrentUser();
        if ($user==null){
            $result = [
                'error'=>true,
                'messages'=> 'Користувач не аутентифікований'
            ];
            return $result;
        }
        if(empty($text)){
            $result = [
                'error'=>true,
                'messages'=> 'Поле "Текст" не може бути порожнім'
            ];
            return $result;
        }
        $Comment['data_time'] =date('Y-m-d H:i:s');
        $Comment['user_id'] =$user['id'];
        $Comment['news_id'] =$newsId;
        $Comment['text'] =$text;
        $id = \core\Core::getInstance()->getDB()->insert('comments',$Comment);
        return true;
    }
    public function ChangePhoto($id,$file){
        $folder = 'files/news/';
        $news = $this->GetNewsByID($id);
        if(is_file($folder.$news['photo'])&& is_file($folder.$file))
            unlink($folder.$news['photo']);
        $news['photo'] = $file;
        $this->UpdateNews($news, $id);
    }
    public function AddNews($row){
        $userModel = new \models\Users();
        $user = $userModel->GetCurrentUser();
        if ($user==null){
            $result = [
                'error'=>true,
                'messages'=> ['Користувач не аутентифікований']
            ];
            return $result;
        }
        $validateResult = $this->Validate($row);
        if (is_array($validateResult)){
            $result = [
                'error'=>true,
                'messages'=> $validateResult
            ];
            return $result;
        }
        $fields = ['title', 'short_text', 'text', 'photo'];
        $rowFiltered = Utils:: ArrayFilter($row,$fields);
        $rowFiltered['datetime'] =date('Y-m-d H:i:s');
        $rowFiltered['user_id'] =$user['id'];
        $rowFiltered['photo'] ='...photo...';
        $id = \core\Core::getInstance()->getDB()->insert('news',$rowFiltered);
        return [
            'error'=>false,
            'id' => $id
        ];
    }
    public function GetLastNews($count){
        return \core\Core::getInstance()->getDB()->select('news','*',null,['datetime'=>'DESC'],$count,null);
    }
    public function GetNewsByID($id){
        $news = \core\Core::getInstance()->getDB()->select('news','*',['id'=>$id]);
        if(!empty($news))
            return $news[0];
        else
            return null;
    }
    public function GetCommentsByID($id){
        $news = \core\Core::getInstance()->getDB()->select('comments','*',['id'=>$id]);
        if(!empty($news))
            return $news[0];
        else
            return null;
    }
    public function UpdateNews($row,$id){
        $userModel = new \models\Users();
        $user = $userModel->GetCurrentUser();
        if ($user==null)
            return false;
        $validateResult = $this->Validate($row);
        if (is_array($validateResult))
            return $validateResult;
        $fields = ['title', 'short_text', 'text', 'photo'];
        $rowFiltered = Utils:: ArrayFilter($row,$fields);
        $rowFiltered['datetime_lastedit'] =date('Y-m-d H:i:s');
        \core\Core::getInstance()->getDB()->update('news',$rowFiltered,['id'=>$id]);
        return true;
    }
    public function DeleteNews($id){
        $news = $this->GetNewsByID($id);
        $userModel = new \models\Users();
        $user = $userModel->GetCurrentUser();
        if( empty($news) || empty($user) || $user['$id'] != $news['$user_id'])
            return false;
        \core\Core::getInstance()->getDB()->delete('news',['id'=>$id]);
        return true;
    }
    public function DeleteComment($id){
        \core\Core::getInstance()->getDB()->delete('comments',['id'=>$id]);
        return true;
    }
    public function Validate($row){
        $errors = [];
        if(empty($row['title']))
            $errors [] = 'Поле "Заголовок новини" не може бути порожнім';

        if(empty($row['short_text']))
            $errors [] = 'Поле "Короткий текст новини" не може бути порожнім';

        if(empty($row['text']))
            $errors [] = 'Поле "Повний текст новини" не може бути порожнім';

        if(count($errors)>0)
            return $errors;
        else
            return true;
    }
}