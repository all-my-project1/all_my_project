<?php
$userModel = new \models\Users();
$user = $userModel->GetCurrentUser();
?>
<!DOCTYPE html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="/css/styles.css" type="text/css" rel="stylesheet">
    <title><?=$MainTitle ?></title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light p-0">
    <nav class="navbar navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="/">
            <img src="/files/logo.png" alt="" width="200" height="100" class="d-inline-block align-text-top">
        </a>
    </div>
    </nav>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-link" aria-current="page" href="/">Головна</a>
                <a class="nav-link" href="/news">Блог</a>
                <a class="nav-link" href="/contacts">Контакти</a>
                <? if(!$userModel->IsUserAuthenticated()) : ?>
                <a class="btn btn-outline-primary me-2" href="/users/login ">Увійти</a>
                <? else: ?>
                    <span class="nav-link"><?=$user['login'] ?></span>
                    <a class="btn btn-outline-primary me-2" href="/users/logout">Вийти</a>
                <? endif; ?>
            </div>
        </div>
    </div>
</nav>
    <? if(!empty($MessageText)) :?>
    <div class="alert alert-<?=$MessageClass ?>" role="alert">
        <?=$MessageText ?>
    </div>
    <? endif; ?>
    <? ?>
    <?=$PageContent ?>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<? if($userModel->IsUserAuthenticated()) :?>
    <script src="/alien/build/ckeditor.js"></script>
    <script>
        let editors = document.querySelectorAll('.editor');
        for(let i in editors)
        {
        ClassicEditor
            .create( editors[i], {
                licenseKey: '',
            } )
            .then( editor => {
                window.editor = editor;
            } )
            .catch( error => {
                console.error( 'Oops, something went wrong!' );
                console.error( 'Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:' );
                console.warn( 'Build id: s0cjh1f5177-4oohzzjormdp' );
                console.error( error );
            } );
        }
    </script>
<? endif;?>
</body>
</html>