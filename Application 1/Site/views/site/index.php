<div id="intro" class="bg-image shadow-2-strong">
        <div class="mask" style="background-image: url('/files/bg.jpg') ;background-size: 100%;height: 100vh;">
            <div class="container d-flex align-items-center justify-content-center text-center h-100">
                <div class="text-white">
                    <h1 class="mb-3">БУХГАЛТЕРСЬКИЙ АУТСОРСІНГ</h1>
                    <h5 class="mb-4">Наша спеціалізація дозволить Вам якісно вирішувати нагальні питання у сфері податків не тримаючи штат спеціальних працівників</h5>
                    <a class="btn btn-outline-light btn-lg m-2" href="/users/register" role="button"
                       rel="nofollow">Реєстрація</a>
                    <a class="btn btn-outline-light btn-lg m-2" href="/news"
                       role="button">Блог</a>
                </div>
            </div>
        </div>
    </div>
<main class="mt-5">
    <div class="container">
        <section>
            <div class="row">
                <div class="col-md-6 gx-5 mb-4">
                    <div class="bg-image hover-overlay ripple shadow-2-strong rounded-5" data-mdb-ripple-color="light">
                        <img src="/files/homepage/4.jpg" class="img-fluid" />
                        <a href="#!">
                            <div class="mask" style="background-color: rgba(251, 251, 251, 0.15);"></div>
                        </a>
                    </div>
                </div>
                <div class="col-md-6 gx-5 mb-4">
                    <h4><strong>Про нас</strong></h4>
                    <p class="text-muted">
                        Наша компанія Legal Tax створена з метою надання якісного сервісу у сфері бухгалтерського аутсорсингу.
                        Маючи великий практичний досвід ми постійно вдосконалюємо свої знання у таких динамічних галузях.
                        Ось чому репутація надійних партнерів для багатьох представників українського бізнесу як у Житомирі так і за його межами - стала запорукою нашого успіху.
                        Свої зусилля ми зосередили на таких напрямках роботи: Будемо раді надати консультацію по будь...
                    </p>
                </div>
            </div>
        </section>
        <hr class="my-5" />
        <section class="text-center">
            <h4 class="mb-5"><strong>Найпопулярніші послуги</strong></h4>

            <div class="row">
                <div class="col-lg-4 col-md-12 mb-4">
                    <div class="card">
                        <div class="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                            <img src="/files/homepage/3.jpg" class="img-fluid" />
                            <a href="#!">
                                <div class="mask" style="background-color: rgba(251, 251, 251, 0.15);"></div>
                            </a>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Зіставлення та подача звіту</h5>
                            <p class="card-text">
                                Єдине вікно подання електронної звітності Програмне забезпечення «Спеціалізоване клієнтське програмне забезпечення д...
                            </p>
                            <a href="/news" class="btn btn-primary">Перейти до блогу</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="card">
                        <div class="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                            <img src="/files/homepage/2.jpg" class="img-fluid" />
                            <a href="#!">
                                <div class="mask" style="background-color: rgba(251, 251, 251, 0.15);"></div>
                            </a>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Кадровий облік</h5>
                            <p class="card-text">
                                Кадровий облік - важлива складова діловодства будь-якого підприємства. Про тонкощі організації цього процесу і ...
                            </p>
                            <a href="/news" class="btn btn-primary">Перейти до блогу</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="card">
                        <div class="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                            <img src="/files/homepage/1.jpg" class="img-fluid" />
                            <a href="#!">
                                <div class="mask" style="background-color: rgba(251, 251, 251, 0.15);"></div>
                            </a>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Бухгалтерський аутсорсінг</h5>
                            <p class="card-text">
                                Наша спеціалізація дозволить Вам якісно вирішувати нагальні питання у сфері податків не тримаючи штат спеціаль...
                            </p>
                            <a href="/news" class="btn btn-primary">Перейти до блогу</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <hr class="my-5"/>