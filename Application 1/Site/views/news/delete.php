<p>
    Ви дійсно бажаете видалити цю новину <b><?=$model['title']?></b>?
</p>
<p>
    <a class="btn btn-danger me-2" href="/news/delete?id=<?=$model['id']?>&confirm=yes">Видалити</a>
    <a class="btn btn-primary me-2" href="<?=$_SERVER['HTTP_REFERER']?>">Відмінити</a>
</p>