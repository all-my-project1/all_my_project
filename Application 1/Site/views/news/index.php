<div class="container">
    <div class="d-flex flex-row text-center mt-3">
    <div class="container">
        <div class="row row-cols-3 mb-3">

            <?php
            $userModel = new \models\Users();
            $user = $userModel->GetCurrentUser();
            foreach ($lastNews as $news) : ?>
                <div class="col card">
                    <div class="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                        <img src="/files/news/<?=$news['photo']?>" class="img-fluid" />
                        <a href="#!">
                            <div class="mask" style="background-color: rgba(251, 251, 251, 0.15);"></div>
                        </a>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title"><?=$news['title']?></h5>
                        <p class="card-text">
                            <?=$news['short_text']?>
                        </p>
                    </div>
                    <div class="btn-group-vertical mb-2">
                                <? if($user['access']==1) :?>
                                    <a href="/news/edit?id=<?=$news['id']?>" class="btn btn-outline-success">Редагувати</a>
                                    <a href="/news/delete?id=<?=$news['id']?>" class="btn btn-outline-danger">Видалити</a>
                                <? endif; ?>
                        <a href="/news/view?id=<?=$news['id']?>" class="btn btn-outline-secondary">Переглянути новину</a>
                </div>
                </div>
            <?php endforeach; ?>
            </div>
        <div class="btn mb-2 ">
            <? if($user['access']==1) :?>
                <a href="/news/add" class="btn btn-outline-secondary" style="border-radius:50%;">+</a>
            <? endif; ?>
        </div>

    </div>
</div>
</div>
