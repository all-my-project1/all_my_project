<?php
$userModel = new \models\Users();
$user = $userModel->GetCurrentUser(); ?>
<div class="container">
    <h2><?=$model['title']?></h2>
    <p><img src="/files/news/<?=$model['photo']?>" alt="Лейтенант Бокатуев" width="40%" style="float:left;margin: 7px 7px 7px 0;">
        <?=$model['text']?></p>
    <div class="col-sm-5 col-md-6 col-12 pb-4 mt-3">
        <form method="post">
            <h3>Додайте коментар</h3>
            <div class="form-floating">
                <textarea name="text" class="form-control" placeholder="Leave a comment here" id="floatingTextarea2" style="height: 100px"></textarea>
                <label for="floatingTextarea2">Ваш коментар</label>
            </div>
            <button type="submit" class="mt-3 btn btn-secondary ">Додати коментар</button>
        </form>
        <h1>Коментарі</h1>
        <? if(empty($comments)) :?>
        <h3>Немає коментарів</h3>
        <?else:?>
        <?foreach ($comments as $comment) :?>
        <div class="comment mt-4 text-justify float-left rounded border border-secondary">
            <div class="rounded border border-secondary">
            <label class="fs-5 fw-bold"><?=$comment['name']?></label> <label> <?=$comment['data_time']?></label>
                <? if($user['id']==$comment['user_id']) :?>
                <a href="/comments/delete?id=<?=$comment['id']?>" class="btn btn-secondary ">Видалити коментар</a>
                <? endif; ?>
            </div>
            <p><?=$comment['text']?></p>
        </div>
        <? endforeach;?>
        <? endif;?>
    </div>
</div>