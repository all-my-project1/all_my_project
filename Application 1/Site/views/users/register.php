<div class="container mt-2">
    <form method="post" action="">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card-group mb-3">
                    <div class="card p-4">
                        <div class="card-body">
                            <h1>Реєстрація</h1>
                            <p class="text-muted">Реєстрація акаунту</p>
                            <div class="input-group mb-3">
                                <input type="text" name="lastname" value="<?=$_POST['lastname']?>"
                                       class="form-control" id="lastname" placeholder="Прізвище">
                            </div>
                            <div class="input-group mb-3">
                                <input type="text" name="firstname" value="<?=$_POST['firstname']?>"
                                       class="form-control" id="firstname" placeholder="Ім'я">
                            </div>
                            <div class="input-group mb-3">
                                <input type="email" name="login" value="<?= $_POST['login']?>"
                                       class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Логін (email)">
                            </div>
                            <div class="input-group mb-3">
                                <input type="password" name="password"  class="form-control" id="exampleInputPassword1"placeholder="Пароль">
                            </div>
                            <div class="input-group mb-3">
                                <input type="password" name="password2"  class="form-control" id="exampleInputPassword1"placeholder="Пароль(ще раз)">
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <button type="submit" class="btn btn-primary px-4">Зареєструватися</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card text-white bg-primary py-5 d-md-down-none" style="width:44%">
                        <div class="card-body text-center">
                            <div>
                                <h2>Бажаєте увійти у свій акаунт?</h2>
                                <p>На цьому сайті ви можете ознайомитись з корисною інформацією про бухгалтерські послуги та де їх отримати</p>
                                <a href="/users/login" class="btn btn-primary active mt-3">Увійти</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>