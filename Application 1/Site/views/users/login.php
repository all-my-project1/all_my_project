
<div class="container mt-5">
    <form method="post" action="">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-group mb-0">
                <div class="card p-4">
                    <div class="card-body">
                        <h1>Увійти</h1>
                        <p class="text-muted">Увійдіть у свій обліковий запис</p>
                        <div class="input-group mb-3">
                            <input name="login" type="email" class="form-control" placeholder="Логін (email)">
                        </div>
                        <div class="input-group mb-4">
                            <input name="password" type="password" class="form-control" placeholder="Пароль">
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <button type="submit" class="btn btn-primary px-4">Увійти</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card text-white bg-primary py-5 d-md-down-none" style="width:44%">
                    <div class="card-body text-center">
                        <div>
                            <h2>Не маєте акаунту?</h2>
                            <p>Зареєструйтесь на нашому сайті та слідкуйте за новими блогами</p>
                            <a href="/users/register" class="btn btn-primary active mt-3">Зареєструватисья</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>
