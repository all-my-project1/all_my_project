<section>
    <div class="row">
        <div class="col-md-6 gx-5 mb-4">
            <div class="bg-image hover-overlay ripple shadow-2-strong rounded-5" data-mdb-ripple-color="light">
                <img src="/files/contact.jpg" class="img-fluid" />
                <a href="#!">
                    <div class="mask" style="background-color: rgba(251, 251, 251, 0.15);"></div>
                </a>
            </div>
        </div>

        <div class="col-md-6 gx-5 mb-4">
            <h4><strong>Як з нами звязатися:</strong></h4>
            <p class="text-muted">
                <br>Аутсорсінгова компанія “Legal tax”
                <br>Наша адреса – м. Житомир, вулиця ********, ***
                <br>Керівник відділу фінансових послуг
                <br>********** ***
                <br>(***) *** ** **
                <br>*******@gmail.com
            </p>
        </div>
    </div>
</section>