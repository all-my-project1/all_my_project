<?php

namespace controllers;

use core\Controller;

class Users extends Controller
{

    protected $usersModel;

    function __construct(){
        $this->usersModel= new \models\Users();
    }
    function actionLogout(){
        $title = 'Вихід з акаунту';
        unset($_SESSION['user']);
        return $this->renderMessage('ok','Ви вийшли з акаунту',null,
            [
                'MainTitle'=>$title,
                'PageTitle'=>$title
            ]);
    }
    function actionLogin(){
        $title = 'Вхід на сайт';
        if(isset($_SESSION['user']))
            return $this->renderMessage('ok','Ви вже увійшли на сайт',null,
                [
                    'MainTitle'=>$title,
                    'PageTitle'=>$title
                ]);
        if($this->isPost()){
            $login = $_POST['login'];
            $password = $_POST['$password'];
            $user=$this->usersModel->AuthUser($_POST['login'],$_POST['password']);
            if (!empty($user)){
                $_SESSION['user']=$user;
                return $this->renderMessage('ok','Ви успішно увійшли в обліковий запис',null,
                    [
                        'MainTitle'=>$title,
                        'PageTitle'=>$title
                    ]);
            }
            else
                return $this->render('login',null,
                    [
                        'PageTitle'=>$title,
                        'MainTitle'=>$title,
                        'MessageText'=>'Неправильний логін або пароль',
                        'MessageClass'=>'danger',
                    ]);
        }else  {
            $params = [
                'PageTitle'=>$title,
                'MainTitle'=>$title
            ];

            return $this->render('login',null, $params);
        }
    }

    function actionRegister(){
        if($this->isPost()){
            $result = $this->usersModel->AddUser($_POST);
            if($result===true){
                return $this->renderMessage('ok','Користувач успішно зареєстрований',null,
                    [
                        'MainTitle'=>'Реєстрація на сайті',
                        'PageTitle'=>'Реєстрація на сайті'
                    ]);
            }
            else{
                $message= implode('<br/>', $result);
                return $this->render('register',null,
                    [
                        'PageTitle'=>'Реєстрація на сайті',
                        'MainTitle'=>'Реєстрація на сайті',
                        'MessageText'=>$message,
                        'MessageClass'=>'danger',
                ]);
            }
        }else  {
            $params = [
                'PageTitle'=>'Реєстрація на сайті',
                'MainTitle'=>'Реєстрація на сайті'
            ];
            return $this->render('register',null, $params);
        }
    }
}