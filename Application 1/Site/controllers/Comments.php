<?php

namespace controllers;

use core\Controller;

class Comments extends Controller
{
    public function actionDelete(){
        $id=$_GET['id'];
        if (!isset($id)){
            header($_SERVER['HTTP_REFERER']);
        }
        $userModel = new \models\Users();
        $newsModel = new \models\News();
        $user = $userModel->GetCurrentUser();
        $comment = $newsModel->GetCommentsByID($id);
        $title = 'Видалення новини';
        if ($user['id']==$comment['user_id']){
            $newsModel->DeleteComment($id);
            return $this->renderMessage('ok','Коментар успішно видалено',null,
                    [
                        'MainTitle'=>$title,
                    ]);
        }
        return $this->renderMessage('error','Помилка видалння коментаря',null,
            [
                'MainTitle'=>$title,
            ]);
    }
}