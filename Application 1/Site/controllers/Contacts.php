<?php

namespace controllers;

use core\Controller;

class Contacts extends Controller
{
    public function actionIndex()
    {
        return $this->render('contacts', null, [
            'MainTitle' => 'Контакти',
            'PageTitle' => 'Контакти'
        ]);
    }
}