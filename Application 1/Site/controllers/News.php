<?php

namespace controllers;
use core\Controller;
use http\Header;

/**
 * Контролер для модуля News
 */
class News extends Controller
{
    protected $user;
    protected $newsModel;
    protected $userModel;
    function __construct(){
        $this->userModel = new \models\Users();
        $this->newsModel = new \models\News();
        $this->user =  $this->userModel->GetCurrentUser();
    }
    /**
     * Відображення початкової сторінки модуля
     */
    public function actionIndex(){
        global $Config;
        $title="Новини";
        $lastNews= $this->newsModel->GetLastNews($Config['NewsCount']);
        return $this->render('index',['lastNews'=>$lastNews],
            [
                'MainTitle'=>$title,
                'PageTitle'=>$title
            ]);
    }

    /**
     *  Перегляд новини
     */
    public function actionView(){
        $id=$_GET['id'];
        $news = $this->newsModel->GetNewsByID($id);
        $title = $news['title'];
        if ($this->isPost()){
            if(is_array($this->newsModel->AddNewsComments($id,$_POST['text']))){
                return $this->renderMessage('error',$this->newsModel->AddNewsComments($id,$_POST['text'])['messages'],null,
                    [
                        'MainTitle'=>$title,
                        'PageTitle'=>$title
                    ]);
            }
        }
        $comments = $this->newsModel->NewsComments($id);
        return $this->render('view',['model'=>$news,'comments'=>$comments],
            [
                'MainTitle'=>$title,
                'PageTitle'=>$title
            ]);
    }

    /**
     * Додавання новини
     */
    public function actionAdd(){
        $titleForbidden = 'Доступ заборонено';
        if($this->userModel->GetCurrentUser()['access']!=1)
            return $this->render('forbidden',null,
                [
                    'MainTitle'=>$titleForbidden,
                    'PageTitle'=>$titleForbidden
                ]);
        $title = 'Додавання новини';
        if ($this->isPost()){
            $result = $this->newsModel->AddNews($_POST);
        if($result['error'] === false){
            $allowed_types = ['image/png', 'image/jpeg'];
            if(is_file($_FILES['file']['tmp_name'])&& in_array($_FILES['file']['type'],$allowed_types)){
                switch ($_FILES['file']['type']){
                    case 'image/png' :
                        $extension = 'png';
                        break;
                    default:
                        $extension = 'jpg';
                }
                $name = $result['id'].'_'.uniqid().'.'.$extension;
                move_uploaded_file($_FILES['file']['tmp_name'],'files/news/'.$name);
                $this->newsModel->ChangePhoto($result['id'], $name);
            }
            return $this->renderMessage('ok','Новину успішно додано',null,
                [
                    'MainTitle'=>$title,
                    'PageTitle'=>$title
                ]);
        }
        else {
            $message = implode('<br/>', $result['messages']);
            return $this->render('form', ['model'=>$_POST],
                [
                    'PageTitle' => $title,
                    'MainTitle' => $title,
                    'MessageText' => $message,
                    'MessageClass' => 'danger',
                ]);
        }
        }else
        return $this->render('form',['model'=>$_POST],
            [
                'MainTitle'=>$title,
                'PageTitle'=>$title
            ]);
    }

    /**
     * Редагування новин
     */
    public function actionEdit(){
        $id = $_GET['id'];
        $news = $this ->newsModel->GetNewsByID($id);
        $titleForbidden = 'Доступ заборонено';
        if(empty($this->user) || $news['user_id'] !=$this->userModel->GetCurrentUser()['id'])
            return $this->render('forbidden',null,
                [
                    'MainTitle'=>$titleForbidden,
                    'PageTitle'=>$titleForbidden
                ]);
        $title = 'Редагування новини';
        if ($this->isPost()){
            $result = $this->newsModel->UpdateNews($_POST, $id);
            if($result === true){
                $allowed_types = ['image/png', 'image/jpeg'];
                if(is_file($_FILES['file']['tmp_name'])&& in_array($_FILES['file']['type'],$allowed_types)){
                    switch ($_FILES['file']['type']){
                        case 'image/png' :
                            $extension = 'png';
                            break;
                        default:
                            $extension = 'jpg';
                    }
                    $name = $id.'_'.uniqid().'.'.$extension;
                    move_uploaded_file($_FILES['file']['tmp_name'],'files/news/'.$name);
                    $this->newsModel->ChangePhoto($id, $name);
                }
                return $this->renderMessage('ok','Новину успішно відредаговано',null,
                    [
                        'MainTitle'=>$title,
                        'PageTitle'=>$title
                    ]);
            }
            else {
                $message = implode('<br/>', $result);
                return $this->render('form', ['model'=>$news],
                    [
                        'PageTitle' => $title,
                        'MainTitle' => $title,
                        'MessageText' => $message,
                        'MessageClass' => 'danger',
                    ]);
            }
        }else
            return $this->render('form',['model'=>$news],
                [
                    'MainTitle'=>$title,
                    'PageTitle'=>$title
                ]);
    }


    /**
     * Видалення новини
     */
    public function actionDelete(){
        $title = 'Видалення новини';
        $id = $_GET['id'];
        if(isset($_GET['confirm']) && $_GET['confirm'] == 'yes'){
            if($this->newsModel->DeleteNews($id))
                header('Location: /news/');
            else
                return $this->renderMessage('error','Помилка видалення новини',null,
                    [
                        'MainTitle'=>$title,
                        'PageTitle'=>$title
                    ]);
        }
        $news=$this->newsModel->GetNewsByID($id);
        return $this->render('delete',['model'=>$news],
            [
                'MainTitle'=>$title,
                'PageTitle'=>$title
            ]);
    }
}